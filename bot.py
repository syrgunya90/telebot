import telebot
import youtube_dl
from telebot import types

bot = telebot.TeleBot('1159648893:AAH8nqdHEVGMgAP2BXC0jZQn4RvmBnbGwiI')

@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Привет, для скачивания дай урл видео с Youtube')

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    if message.text.startswith('https://www.youtube.com/') or message.text.startswith('https://youtu.be/'):
        bot.send_message(message.chat.id, "Подождите идет обработка")
        options = {  # Настройки youtube_dl
            'outtmpl': '%(title)s-%(id)s.%(ext)s',
            'format': 'best'
        }
        with youtube_dl.YoutubeDL(options) as ydl:
            r = ydl.extract_info(message.text, download=False)  # Вставляем нашу ссылку с ютуба
            videoUrl = r['url']  # Получаем прямую ссылку на скачивание видео
            markup = types.InlineKeyboardMarkup()
            btn_my_site = types.InlineKeyboardButton(text='Скачать', url=videoUrl)
            markup.add(btn_my_site)
            bot.send_message(message.chat.id, "Нажми на кнопку скачать.", reply_markup=markup)

    else:
        bot.send_message(message.from_user.id, 'урл не сайта Youtube или не корректный')


bot.polling()